import googlemaps
from datetime import datetime
from googlemaps import elevation
import time
import matplotlib.pyplot as plt

elevation_key = "AIzaSyBPAUmtTIvx9YKT0uaPa4orELWbL4zeNdA"
directions_key = "AIzaSyDYaZt_NOsM_-llKIsyfEh-PWUlIQDzAKI"

gmaps = googlemaps.Client(key=directions_key)

def dem_lats_bro(gmaps, start, end):
	now = datetime.now()
	directions_result = gmaps.directions(start,
	                                     end,
	                                     mode="bicycling",
	                                     departure_time=now)

	temp = []

	if directions_result != []:
		for item in  directions_result[0]["legs"][0]["steps"]:
			try:
				if item["start_location"] not in temp:
					temp.append(item["start_location"])
				if item["end_location"] not in temp:
					temp.append(item["end_location"])
			except: 
				pass
	else: 
		temp = ["Error pls rerun"]

	return temp

def polywhatnow(start, end, gmaps = gmaps):
	now = datetime.now()
	directions_result = gmaps.directions(start,
	                                     end,
	                                     mode="bicycling",
	                                     departure_time=now)
	temp = []
	print directions_result
	if directions_result != []:

		for item in  directions_result[0]:
			print item
	else: 
		temp = ["Error pls rerun"]
	return temp

def rel_ele(journey, gmaps = gmaps, sample = 100):

	elev_obj = elevation.elevation_along_path(gmaps, journey, sample)
	last = elev_obj[0]["elevation"]
	rel_ele = []

	for sample in elev_obj:
		rel_ele.append((last-sample["elevation"])*-1)
		last = sample["elevation"]

	return rel_ele

def ele(journey, gmaps = gmaps, sample = 100):

	elev_obj = elevation.elevation_along_path(gmaps, journey, sample)
	rel_ele = []

	for sample in elev_obj:
		rel_ele.append(sample["elevation"])

	return rel_ele

journey = dem_lats_bro(gmaps, "BN1 4SG","BN1 1EG")
relitive_elevation = rel_ele(journey)

ebike = [x/2 for x in relitive_elevation]

# for item in zip(relitive_elevation, ebike):
# 	print item 

plt.plot(relitive_elevation)
plt.plot(ebike)

plt.show()